package config

import (
	"flag"
	"log"

	"github.com/spf13/pflag"
	"github.com/spf13/viper"
)

type DBConfig struct {
	Host     string `yaml:"host"`
	Port     string `yaml:"port"`
	User     string `yaml:"user"`
	Password string `yaml:"password"`
	DBName   string `yaml:"dbname"`
}

// Config holds configuration details of the application
type Config struct {
	DBConfig  DBConfig
	PublicKey string
}

func init() {
	flag.String("db_host", "", "database host")
	flag.String("db_port", "", "database port")
	flag.String("db_name", "", "database name")
	flag.String("db_user", "", "database username")
	flag.String("db_password", "", "database password")
	flag.String("publickey", "", "public key to verify messages")

	pflag.CommandLine.AddGoFlagSet(flag.CommandLine)
	pflag.Parse()
	viper.BindPFlags(pflag.CommandLine)
}

func NewConfig() *Config {
	viper.SetConfigName("config")
	viper.SetConfigType("yaml")
	viper.AddConfigPath("./config/")

	viper.SetEnvPrefix("VERIFY")

	if err := viper.ReadInConfig(); err != nil {
		if _, ok := err.(viper.ConfigFileNotFoundError); ok {
			log.Println("Config file not found")
		} else {
			log.Println("Config file parse error", err)
		}
	}
	viper.AutomaticEnv()

	config := &Config{
		DBConfig: DBConfig{
			Host:     viper.GetString("db_host"),
			Port:     viper.GetString("db_port"),
			DBName:   viper.GetString("db_name"),
			User:     viper.GetString("db_user"),
			Password: viper.GetString("db_password"),
		},
		PublicKey: viper.GetString("publickey"),
	}

	return config
}