package db

import (
	"context"
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
	"gitlab.com/interview-test-group/verify-service/internal/config"
	"gitlab.com/interview-test-group/verify-service/usecase"
	"go.uber.org/fx"
	"log"
)

func CreateConnectionString(host string, port string, user, password, dbname string) string {
	return fmt.Sprintf(
		"host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
		host, port, user, password, dbname,
	)
}

func NewPostgresClient(connString string) (*sql.DB, error) {
	dbtx, err := sql.Open("postgres", connString)

	if err != nil {
		log.Printf("Database connection error %v", err)
		return nil, err
	}
	return dbtx, nil
}

// NewPostgresClientProvider generator
func NewPostgresClientProvider(lc fx.Lifecycle, conf *config.Config) (*sql.DB, error) {
	dbConfig := conf.DBConfig
	connString := CreateConnectionString(dbConfig.Host,
		dbConfig.Port,
		dbConfig.User,
		dbConfig.Password,
		dbConfig.DBName)
	conn, err := NewPostgresClient(connString)
	if err != nil {
		log.Printf("couldn't create new postgres client %v", err)
		return nil, err
	}
	lc.Append(fx.Hook{
		OnStart: func(ctx context.Context) error {

			if err := Migrate(conn); err != nil {
				log.Printf("Database Migrate error %v", err)
				return err
			}
			log.Println("Database Migrated")
			return nil
		},

		OnStop: func(ctx context.Context) error {
			err := conn.Close()
			if err != nil {
				log.Printf("couldn't close db connection %v", err)
				return err
			}
			return nil
		},
	})
	return conn, nil
}

func Migrate(conn *sql.DB) error {
	_, err := conn.Exec(`CREATE TABLE IF NOT EXISTS sources(
                       id     uuid  PRIMARY KEY NOT NULL DEFAULT uuid_generate_v4(),
                       source varchar ,
                       message varchar ,
                       sign TEXT ,
                       created_at TIMESTAMP WITH TIME ZONE DEFAULT NOW()
)`)
	if err != nil {
		return err
	}
	return nil
}

type PostgresVerifyRepository struct {
	c *sql.DB
}

func NewPostgresVerifyRepository(c *sql.DB) usecase.VerifyRepository {
	return &PostgresVerifyRepository{c: c}
}

func (p PostgresVerifyRepository) Save(ctx context.Context, v *usecase.Verify) error {
	stmt, err := p.c.Prepare( "insert into sources (source, message, sign) values ($1, $2, $3)")
	if err != nil {
		return err
	}
	defer stmt.Close()

	_, err = stmt.Exec(v.Source, v.Message, v.Sign)
	if err!=nil {
			return err
	}

	return nil
}
