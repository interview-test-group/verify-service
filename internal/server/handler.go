package server

import (
	"github.com/labstack/echo"
	"github.com/labstack/gommon/log"
	"gitlab.com/interview-test-group/verify-service/usecase"
	"net/http"
)

type Verifier interface {
	Verify(c echo.Context) error
}

type handler struct {
	uc usecase.VerifyUsecase
}

func NewHandler(uc usecase.VerifyUsecase) Verifier {
	return &handler{
		uc: uc}
}

func (h *handler) Verify(c echo.Context) error {
	req := &verifyRequest{}
	err := c.Bind(req)
	if err != nil {
		log.Printf("couldn't bind request %v", err)
		return c.JSON(http.StatusBadRequest, err.Error())
	}
	ctx := c.Request().Context()
	verifyReq := &usecase.Verify{
		Source:  req.Source,
		Message: req.Message,
		Sign:    req.Sign,
	}
	err = h.uc.Verify(ctx, verifyReq)
	if err != nil {
		log.Printf("verify error: %v", err)
		return c.JSON(http.StatusBadRequest, verifyResponse{Success: false})
	}
	return c.JSON(http.StatusOK, verifyResponse{Success: true})
}
