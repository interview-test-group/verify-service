package server

type verifyRequest struct {
	Source  string `json:"source"`
	Message string `json:"message"`
	Sign    string `json:"sign"'`
}

type verifyResponse struct {
	Success bool `json:"success"`
}
