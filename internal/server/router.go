package server

import (
	"net/http"

	"github.com/labstack/echo"
)

func Register(v Verifier, group *echo.Group) {
	group.POST("verify", v.Verify)
}

// RegisterRoutes registers route groups and independent routes to the engine
func RegisterRoutes(engine *echo.Echo, v Verifier) {
	engine.GET("/ping", func(c echo.Context) error {
		return c.String(http.StatusOK, "pong")
	})
	Register(v, engine.Group("/"))
}
