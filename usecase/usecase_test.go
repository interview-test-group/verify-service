package usecase

import (
	"bytes"
	"crypto"
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha256"
	"crypto/x509"
	"encoding/base64"
	"encoding/pem"
	"fmt"
	"log"
	"testing"
)

func TestVerifyMessage(t *testing.T) {

	// sign
	privateKey, publicKey, err := generateKeyPair(2048)

	if err != nil {
		t.Fatalf("could not generate keypair: %s", err.Error())
	}

	msg := "My message!"
	msgBytes := sha256.Sum256([]byte(msg))

	signature, signErr := rsa.SignPKCS1v15(rand.Reader, privateKey, crypto.SHA256, msgBytes[:])

	if signErr != nil {
		t.Errorf("Could not sign message:%s", signErr.Error())
	}

	b64sign := base64.StdEncoding.EncodeToString(signature)

	block := &pem.Block{
		Type:  "PUBLIC KEY",
		Bytes: x509.MarshalPKCS1PublicKey(publicKey),
	}

	var p bytes.Buffer
	if err := pem.Encode(&p, block); err != nil {
		log.Fatal(err)
	}

	// verify
	fmt.Println(p.String(), "publickey!!!")
	fmt.Println(b64sign, "signature!!!")
	fmt.Println(msg, "message!!!")
	verifyErr := VerifyMessage(p.String(), b64sign, msg)
	if verifyErr != nil {
		t.Errorf("Verification failed: %s", verifyErr)
	}
}

// GenerateKeyPair generates a new key pair
func generateKeyPair(bits int) (*rsa.PrivateKey, *rsa.PublicKey, error) {
	r := rand.Reader
	privkey, err := rsa.GenerateKey(r, bits)
	if err != nil {
		return nil, nil, err
	}

	return privkey, &privkey.PublicKey, nil
}
