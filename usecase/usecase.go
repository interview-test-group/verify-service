package usecase

import (
	"context"
	"crypto"
	"crypto/rsa"
	"crypto/sha256"
	"crypto/x509"
	"encoding/base64"
	"encoding/pem"
	"fmt"
	"gitlab.com/interview-test-group/verify-service/internal/config"
)

//Voice ...
type Verify struct {
	Source  string `json:"source"`
	Message string `json:"message"`
	Sign    string `json:"sign"`
}
type VerifyUsecase interface {
	Verify(ctx context.Context, v *Verify) error
}

// VoiceRepository represent the voice's repository contract
type VerifyRepository interface {
	Save(ctx context.Context, v *Verify) error
}

type verifyUsecase struct {
	config *config.Config
	repo   VerifyRepository
}

func NewVerifyUseCase(conf *config.Config, repo VerifyRepository) VerifyUsecase {
	return &verifyUsecase{config: conf, repo: repo}
}

func (v *verifyUsecase) Verify(ctx context.Context, verify *Verify) error {

	err := VerifyMessage(v.config.PublicKey, verify.Sign, verify.Message)
	if err != nil {
		return err
	}
	err = v.repo.Save(ctx, verify)
	if err != nil {
		return err
	}
	return nil
}

func VerifyMessage(rawPubKey, rawSignature, message string) error {
	block, _ := pem.Decode([]byte(rawPubKey))
	if block == nil {
		return fmt.Errorf("Invalid PEM Block")
	}

	pubKey, err := x509.ParsePKCS1PublicKey(block.Bytes)
	if err != nil {
		return fmt.Errorf("ParsePKCS1PublicKey: %e", err)
	}

	signature, err := base64.StdEncoding.DecodeString(rawSignature)
	if err != nil {
		return fmt.Errorf("base64 DecodeString: %e", err)
	}

	hash := sha256.Sum256([]byte(message))

	err = rsa.VerifyPKCS1v15(pubKey, crypto.SHA256, hash[:], signature)
	if err != nil {
		fmt.Println("rsa", err)
		return fmt.Errorf("rsa: %e", err)
	}

	fmt.Println("Successfully verified message with signature and public key")
	return nil
}
