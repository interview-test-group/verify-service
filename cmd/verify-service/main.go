package main

import (
	"context"
	"gitlab.com/interview-test-group/verify-service/internal/config"
	"gitlab.com/interview-test-group/verify-service/internal/db"
	"gitlab.com/interview-test-group/verify-service/internal/server"
	"gitlab.com/interview-test-group/verify-service/usecase"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"net/http"

	"go.uber.org/fx"
)

// NewServer creates and lazily starts an engine by consuming config
func NewServer(lc fx.Lifecycle, conf *config.Config) *echo.Echo {
	e := echo.New()
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowMethods: []string{http.MethodGet, http.MethodHead, http.MethodPut, http.MethodPatch, http.MethodPost, http.MethodDelete},
	}))

	lc.Append(fx.Hook{
		OnStart: func(context.Context) error {
			e.Logger.Info("Server is starting up")
			go e.Start(":1323")
			return nil
		},
		OnStop: func(ctx context.Context) error {
			e.Logger.Info("Server is shutting down")
			return e.Shutdown(ctx)
		},
	})

	return e
}

func main() {
	app := fx.New(
		fx.Provide(
			config.NewConfig,
			db.NewPostgresClientProvider,
			db.NewPostgresVerifyRepository,
			usecase.NewVerifyUseCase,

			server.NewHandler,
			NewServer,
		),
		fx.Invoke(server.RegisterRoutes),
	)

	app.Run()
}
